from docx import Document
from docx.shared import Inches
from docx import Document
from docx.oxml import OxmlElement
from docx.shared import Pt
from docx.oxml.ns import qn
from gen_wordtool import add_heading, add_paragraph, add_list_bullet
from gen_wordtool import add_list_number, add_caption, add_pic
from gen_wordtool import add_separate, add_table, set_table_border
import gen_wordtool

gen_wordtool.doc = Document("demo.docx")

add_heading("開頭", 1)
p = add_paragraph("這是一篇測試文章, ")
p.add_run("中間也可以加粗體").bold = True

add_heading('第一章', 2)
p=add_paragraph('下面是一個列表')
add_list_bullet("蘋果")
add_list_bullet("香蕉")
add_list_bullet("芭樂")
add_separate()

add_paragraph("下面是其編號")
add_list_number("Apple")
add_list_number("Banana")
add_list_number("Fruit")

add_separate()

p = add_pic("img/pic.jpg")
add_caption(p, "自動圖片")

add_separate()

records = (
    (3, '101', 'Spam'),
    (7, '422', 'Eggs'),
    (4, '631', 'Spam, spam, eggs, and spam')
)

table = add_table(rows=1, cols=3)
table.style = "Light Shading Accent 1"
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Qty'
hdr_cells[1].text = 'Id'
hdr_cells[2].text = 'Desc'
for qty, id, desc in records:
    row_cells = table.add_row().cells
    row_cells[0].text = str(qty)
    row_cells[1].text = id
    row_cells[2].text = desc


set_table_border(table)
add_caption(table, "自動表格")
gen_wordtool.doc.add_page_break()

gen_wordtool.doc.save('demo2.docx')
